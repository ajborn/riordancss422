/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;


import Model.MainModel;
import View.BottlePanel;
import View.InventoryView;
import java.awt.Dimension;
import javax.swing.JFrame;

/**
 *
 * @author Austin
 */
public class MainActivity extends MainModel{
    
        public MainActivity(){
          DBController d = new DBController();
           DBController a = new DBController();
          d.getTransactionId();
          a.fillJlist();
       
        }  
           
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MainActivity m = new MainActivity();
        BottlePanel bp = new BottlePanel();
        InventoryView inView = new InventoryView();
        inView.add(bp);
        JFrame.setDefaultLookAndFeelDecorated(false);
        final Dimension maxSize = new Dimension(450, 500);
        inView.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        inView.setSize(850, 600);
        inView.setMaximumSize(maxSize);
        
        inView.setVisible(true);
        
    }
    
}
