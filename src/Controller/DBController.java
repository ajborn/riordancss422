/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.MainModel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Austin
 */
public class DBController extends MainModel {
    //DB host holds the address for the DB on the server   

    private String dbHost = "jdbc:mysql://localhost:3306/css422?" + "user=root&password=";
    //This string holds the statement that will exectue the DB
    int counter = 0;
    private int increaseQuantity;
    private int itemNum;

    public void doIncreaseBottleQuantity(int i, int itemnum, String src) {
        increaseQuantity = i;
        itemNum = itemnum;
        String source = src;
        try {
            Connection conn = DriverManager.getConnection(dbHost);
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            stmt.executeUpdate("UPDATE" + source + "SET Quantity = Quantity + '" + increaseQuantity + "' WHERE ItemNumber = '" + itemNum + "'");
            conn.close();
        } catch (SQLException e) {
            System.err.println(e);
        }

    }

    public void retriveBottleItemInfo(String s) {
        String query = s;
        System.out.println(query);
        ResultSet rsBottles;
        try {
            Connection conn = DriverManager.getConnection(dbHost);
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rsBottles = stmt.executeQuery(query);

            while (rsBottles.next()) {

                //Item Number
                int itemN = rsBottles.getInt(1);
                this.setItemNumber(itemN);
                //Item Description
                String itemDesc = rsBottles.getString(2);
                this.setItemDescription(itemDesc);
                //Item Cost
                float itemC = rsBottles.getFloat(3);
                this.setItemCost(itemC);
                //Item Price
                float itemP = rsBottles.getFloat(4);
                this.setItemPrice(itemP);

                //Item Quantity
                int itemQ = rsBottles.getInt(5);
                this.setItemQuantity(itemQ);

                System.out.println(itemN + itemDesc + itemC + itemP + itemQ);

            }
            rsBottles.close();
            conn.close();
        } catch (SQLException err) {
            System.out.println(err);

        }

    }

    public void retrieveLidInfo(String s) {
        ResultSet rsLids;
        String query = s;
        try {
            Connection conn = DriverManager.getConnection(dbHost);
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rsLids = stmt.executeQuery(query);
            while (rsLids.next()) {

                //Item Number
                int itemN = rsLids.getInt(1);
                this.setItemNumber(itemN);
                //Item Description
                String itemDesc = rsLids.getString(2);
                this.setItemDescription(itemDesc);
                //Item Cost
                float itemC = rsLids.getFloat(3);
                this.setItemCost(itemC);
                //Item Price
                float itemP = rsLids.getFloat(4);
                this.setItemPrice(itemP);

                //Item Quantity
                int itemQ = rsLids.getInt(5);
                this.setItemQuantity(itemQ);

                System.out.println(itemN + itemDesc + itemC + itemP + itemQ);

            }
            rsLids.close();
            conn.close();
        } catch (SQLException err) {
            System.out.println(err);

        }

    }

    public void retrieveCusFabInfo(String s) {
        String query = s;
        ResultSet rsCusFab;

        try {
            Connection conn = DriverManager.getConnection(dbHost);
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

            rsCusFab = stmt.executeQuery(query);

            while (rsCusFab.next()) {

                //Item Number
                int itemN = rsCusFab.getInt(1);
                this.setItemNumber(itemN);
                //Item Description
                String itemDesc = rsCusFab.getString(2);
                this.setItemDescription(itemDesc);
                //Item Cost
                float itemC = rsCusFab.getFloat(3);
                this.setItemCost(itemC);
                //Item Price
                float itemP = rsCusFab.getFloat(4);
                this.setItemPrice(itemP);

                //Item Quantity
                int itemQ = rsCusFab.getInt(5);
                this.setItemQuantity(itemQ);
                System.out.println(itemN + itemDesc + itemC + itemP + itemQ);

            }
            rsCusFab.close();
            conn.close();
        } catch (SQLException err) {
            System.err.println(err);

        }

    }

    public void retrieveFans(String s) {
        String query = s;

        try {
            Connection conn = DriverManager.getConnection(dbHost);
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rsFans = stmt.executeQuery(query);

            while (rsFans.next()) {

                //Item Number
                int itemN = rsFans.getInt(1);
                this.setItemNumber(itemN);
                //Item Description
                String itemDesc = rsFans.getString(2);
                this.setItemDescription(itemDesc);
                //Item Cost
                float itemC = rsFans.getFloat(3);
                this.setItemCost(itemC);
                //Item Price
                float itemP = rsFans.getFloat(4);
                this.setItemPrice(itemP);

                //Item Quantity
                int itemQ = rsFans.getInt(5);
                this.setItemQuantity(itemQ);
                System.out.println(itemN + itemDesc + itemC + itemP + itemQ);
                rsFans.close();
                conn.close();
            }
        } catch (SQLException err) {
            System.err.println(err);

        }
    }

    public void retrieveStentInfo(String s) {
        String query = s;
        try {
            Connection conn = DriverManager.getConnection(dbHost);
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rsFans = stmt.executeQuery(query);

            while (rsFans.next()) {

                //Item Number
                int itemN = rsFans.getInt(1);
                this.setItemNumber(itemN);
                //Item Description
                String itemDesc = rsFans.getString(2);
                this.setItemDescription(itemDesc);
                //Item Cost
                float itemC = rsFans.getFloat(3);
                this.setItemCost(itemC);
                //Item Price
                float itemP = rsFans.getFloat(4);
                this.setItemPrice(itemP);

                //Item Quantity
                int itemQ = rsFans.getInt(5);
                this.setItemQuantity(itemQ);
                System.out.println(itemN + itemDesc + itemC + itemP + itemQ);
                rsFans.close();
                conn.close();
            }
        } catch (SQLException err) {
            System.err.println(err);

        }
    }

    public void retriveStorageInfo(String s) {
        String query = s;
        try {
            Connection conn = DriverManager.getConnection(dbHost);
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rsFans = stmt.executeQuery(query);

            while (rsFans.next()) {

                //Item Number                           
                int itemN = rsFans.getInt(1);
                this.setItemNumber(itemN);
                //Item Description
                String itemDesc = rsFans.getString(2);
                this.setItemDescription(itemDesc);
                //Item Cost
                float itemC = rsFans.getFloat(3);
                this.setItemCost(itemC);
                //Item Price
                float itemP = rsFans.getFloat(4);
                this.setItemPrice(itemP);

                //Item Quantity
                int itemQ = rsFans.getInt(5);
                this.setItemQuantity(itemQ);
                System.out.println(itemN + itemDesc + itemC + itemP + itemQ);
                rsFans.close();
                conn.close();
            }
        } catch (SQLException err) {
            System.err.println(err);

        }
    }

    public void doDecreaseInv(int i, int itemnum, String src) {
        increaseQuantity = i;
        itemNum = itemnum;
        String source = src;
        try {
            Connection conn = DriverManager.getConnection(dbHost);
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            stmt.executeUpdate("UPDATE" + source + "SET Quantity = Quantity - '" + increaseQuantity + "' WHERE ItemNumber = '" + itemNum + "'");
            conn.close();
        } catch (SQLException e) {
            System.err.println(e);
        }
    }

    public void createTransaction(int source, int quantity) {
        try {
            Connection conn = DriverManager.getConnection(dbHost);
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

            stmt.executeUpdate("INSERT INTO transactions (ItemNumber, Quantity)"
                    + "VALUES ('" + source + "','" + quantity + "')");
            conn.close();
        } catch (SQLException e) {
            System.err.println(e);
        }


    }
    public void fillJlist(){
        Integer[] id = new Integer[1000];
          try {
            String query = "SELECT * FROM TransactionID";
            Connection conn = DriverManager.getConnection(dbHost);
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rsRowCount;
            
            
            // counter = getRowCount(rsRowCount);
            rsRowCount = stmt.executeQuery(query);
            
            while(rsRowCount.next()){
                id[0] = rsRowCount.getInt(1);
                
            
          }
            System.out.println("HAHAHA " + id[0]);
            rsRowCount.close();
            conn.close();
            
        } catch (SQLException e) {
            System.err.println(e);
        }
    }

    public void getTransactionId() {

        try {
            String query = "SELECT * TransactionID FROM transactions";
            Connection conn = DriverManager.getConnection(dbHost);
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rsRowCount;
            // counter = getRowCount(rsRowCount);
            rsRowCount = stmt.executeQuery("SELECT COUNT(*) FROM transactions");
            
            rsRowCount.next();
            counter = rsRowCount.getInt(1);
            
           
            rsRowCount.close();
            conn.close();
            this.setTransList(counter);
        } catch (SQLException e) {
            System.err.println(e);
        }
        

    }

    public void getTransactions(int row) {
        String rowNum = String.valueOf(row + 1);
        try {
            String query = "SELECT * FROM transactions WHERE TransactionID = '" + rowNum + "'";
            Connection conn = DriverManager.getConnection(dbHost);
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

            ResultSet rsTrans = stmt.executeQuery(query);

            System.out.println(query);
            while (rsTrans.next()) {

                //Item Number                           
                int transId = rsTrans.getInt(1);
                System.out.println(transId);
                this.setTransNumber(transId);
                //Item Description
                int itemNum = rsTrans.getInt(2);
                System.out.println(itemNum);
                this.setItemNumber(itemNum);
                //Item Cost

                int itemQ = rsTrans.getInt(3);
                this.setItemQuantity(itemQ);

                System.out.println(transId + itemNum + itemQ);
                rsTrans.close();
                conn.close();

            }

        } catch (SQLException e) {
            System.err.println(e);
        }


    }
}
