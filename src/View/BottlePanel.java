/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.MainModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Team A
 */
public class BottlePanel extends javax.swing.JPanel implements ActionListener {

    public String[] data = new String[5];
    public MainModel mainModel = new MainModel();
    private int value;
    private int inumber;
    /**
     * Creates new form BottlePanel
     */
    public BottlePanel() {
        initComponents();
        
        AddInvButton.addActionListener(this);
        AddInvButton2.addActionListener(this);
        AddInvButton3.addActionListener(this);
        AddInvButton4.addActionListener(this);
        AddInvButton5.addActionListener(this);
        AddInvButton6.addActionListener(this);
        removeInvButton.addActionListener(this);
        removeInvButton2.addActionListener(this);
        removeInvButton3.addActionListener(this);
        removeInvButton4.addActionListener(this);
        removeInvButton5.addActionListener(this);
        removeInvButton6.addActionListener(this);
        clearBottles.addActionListener(this);
        clearBottles1.addActionListener(this);
        clearBottles2.addActionListener(this);
        clearBottles3.addActionListener(this);
        clearBottles4.addActionListener(this);
        clearBottles4.addActionListener(this);
        bottlesList.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
               
                if (e.getValueIsAdjusting() == false) {
                    if (bottlesList.getSelectedIndex() == -1) {
                    //No selection, disable fire button.

                    } else if (bottlesList.getSelectedIndex() == 0) {
                        value = 0;
                        mainModel.getItemInformation(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripBottles.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostBottles.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceBottle.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityB.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList.getSelectedIndex() == 1) {
                        value = 1;
                        mainModel.getItemInformation(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripBottles.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostBottles.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceBottle.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityB.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList.getSelectedIndex() == 2) {
                        value = 2;
                        mainModel.getItemInformation(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripBottles.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostBottles.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceBottle.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityB.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList.getSelectedIndex() == 3) {
                        value = 3;
                        mainModel.getItemInformation(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripBottles.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostBottles.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceBottle.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityB.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList.getSelectedIndex() == 4) {
                        value = 4;
                        mainModel.getItemInformation(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripBottles.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostBottles.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceBottle.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityB.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } 
                    
                }
            }
        });
        bottlesList1.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting() == false) {
                    if (bottlesList1.getSelectedIndex() == -1) {
                    //No selection, disable fire button.

                    } else if (bottlesList1.getSelectedIndex() == 0) {
                        value = 0;
                        mainModel.getBottleLidInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber2.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripLids.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostLids.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceLids.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityBL.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList1.getSelectedIndex() == 1) {
                        value = 1;
                        mainModel.getBottleLidInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber2.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripLids.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostLids.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceLids.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityBL.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList1.getSelectedIndex() == 2) {
                        value = 2;
                        mainModel.getBottleLidInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber2.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripLids.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostLids.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceLids.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityBL.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList1.getSelectedIndex() == 3) {
                        value = 3;
                        mainModel.getBottleLidInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber2.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripLids.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostLids.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceLids.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityBL.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList1.getSelectedIndex() == 4) {
                        value = 4;
                        mainModel.getBottleLidInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber2.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripLids.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostLids.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceLids.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityBL.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } 
                    
                }
            }
        }
                );
        bottlesList2.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
               
                if (e.getValueIsAdjusting() == false) {
                    if (bottlesList2.getSelectedIndex() == -1) {
                    //No selection, disable fire button.

                    } else if (bottlesList2.getSelectedIndex() == 0) {
                        value = 0;
                        mainModel.getBottleLidInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber3.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripCusFab.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostCusFab.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceCusFab.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityCF.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList2.getSelectedIndex() == 1) {
                        value = 1;
                        mainModel.getBottleLidInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber3.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripCusFab.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostCusFab.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceCusFab.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityCF.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList2.getSelectedIndex() == 2) {
                        value = 2;
                        mainModel.getBottleLidInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber3.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripCusFab.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostCusFab.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceCusFab.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityCF.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList2.getSelectedIndex() == 3) {
                        value = 3;
                        mainModel.getBottleLidInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber3.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripCusFab.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostCusFab.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceCusFab.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityCF.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList2.getSelectedIndex() == 4) {
                        value = 4;
                        mainModel.getBottleLidInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber3.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripCusFab.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostCusFab.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceCusFab.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityCF.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } 
                    
                }
            }
        });
        bottlesList3.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting() == false) {
                    if (bottlesList3.getSelectedIndex() == -1) {
                    //No selection, disable fire button.

                    } else if (bottlesList3.getSelectedIndex() == 0) {
                        value = 0;
                        mainModel.getFanInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber4.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripFans.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostFans.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceFans.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityF.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList3.getSelectedIndex() == 1) {
                        value = 1;
                        mainModel.getFanInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber4.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripFans.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostFans.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceFans.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityF.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList3.getSelectedIndex() == 2) {
                        value = 2;
                        mainModel.getFanInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber4.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripFans.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostFans.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceFans.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityF.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList3.getSelectedIndex() == 3) {
                        value = 3;
                        mainModel.getFanInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber4.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripFans.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostFans.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceFans.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityF.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList3.getSelectedIndex() == 4) {
                        value = 4;
                        mainModel.getFanInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber4.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripFans.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostFans.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceFans.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityF.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } 
                    
                }
            }
        }
                );
        bottlesList4.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
               
                if (e.getValueIsAdjusting() == false) {
                    if (bottlesList4.getSelectedIndex() == -1) {
                    //No selection, disable fire button.

                    } else if (bottlesList4.getSelectedIndex() == 0) {
                        value = 0;
                        mainModel.getFanInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber5.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripStents.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostStents.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceStents.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityF.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList4.getSelectedIndex() == 1) {
                        value = 1;
                        mainModel.getFanInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber5.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripStents.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostStents.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceStents.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityF.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList4.getSelectedIndex() == 2) {
                        value = 2;
                        mainModel.getFanInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber5.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripFans.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostFans.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceFans.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityF.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList4.getSelectedIndex() == 3) {
                        value = 3;
                        mainModel.getFanInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber5.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripFans.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostFans.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceFans.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityF.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } else if (bottlesList4.getSelectedIndex() == 4) {
                        value = 4;
                        mainModel.getFanInfo(value);
                        //Selection, enable the fire button.
                        data[0] = mainModel.getItemNumber();
                        itemNumber6.setText("Item Number: " + data[0]);
                        data[1] = mainModel.getItemDescription();
                        itemDescripStorage.setText("Item Description: " + data[1]);
                        data[2] = mainModel.getItemCost();
                        itemCostStorage.setText("Item Cost: " + data[2]);
                        data[3] = mainModel.getItemPrice();
                        itemPriceStorage.setText("Item Price: " + data[3]);
                        data[4] = mainModel.getItemQuantity();
                        quantityS.setText("Item Quantity: " + data[4]);
                        System.out.println(data);
                    } 
                    
                }
            }
        });
        transactionsList.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                int list = transactionsList.getSelectedIndex();
                if (e.getValueIsAdjusting() == false) {
                    if (transactionsList.getSelectedIndex() == -1) {
                    //No selection, disable fire button.
                            
                    } else if (transactionsList.getSelectedIndex() == list) {
                        value = list;
                        System.out.println(value);
                        mainModel.getTransactionRow(value);
                        data[0] = mainModel.getTransNumber();
                        transactionId.setText("Trans Id: " +  data[0]);
                        data[1] = mainModel.getItemNumber();
                        itemNumber8.setText("Item Number: " + data[1]);
                        data[2] = mainModel.getItemQuantity();
                        quantityTrans.setText("Quantity Sold: " + data[2]);
                        System.out.println(data);
                    } 
                    
                }
            }
        }
                );
    }
         
 

    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFrame1 = new javax.swing.JFrame();
        jPanel2 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        transTab = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        amountInboundLabel = new javax.swing.JLabel();
        quanityFieldOne = new javax.swing.JTextField();
        AddInvButton = new javax.swing.JButton();
        amountOutboundLabel = new javax.swing.JLabel();
        quanityFieldTwo = new javax.swing.JTextField();
        removeInvButton = new javax.swing.JButton();
        itemNumber = new javax.swing.JLabel();
        itemDescripBottles = new javax.swing.JLabel();
        itemCostBottles = new javax.swing.JLabel();
        itemPriceBottle = new javax.swing.JLabel();
        quantityB = new javax.swing.JLabel();
        clearBottles = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        bottlesList = new javax.swing.JList();
        jPanel3 = new javax.swing.JPanel();
        amountInboundLabel2 = new javax.swing.JLabel();
        quanityFieldOne2 = new javax.swing.JTextField();
        AddInvButton2 = new javax.swing.JButton();
        amountOutboundLabel2 = new javax.swing.JLabel();
        quanityFieldTwo2 = new javax.swing.JTextField();
        removeInvButton2 = new javax.swing.JButton();
        itemNumber2 = new javax.swing.JLabel();
        itemDescripLids = new javax.swing.JLabel();
        itemCostLids = new javax.swing.JLabel();
        itemPriceLids = new javax.swing.JLabel();
        quantityBL = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        bottlesList1 = new javax.swing.JList();
        clearBottles1 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        amountInboundLabel3 = new javax.swing.JLabel();
        quanityFieldOne3 = new javax.swing.JTextField();
        AddInvButton3 = new javax.swing.JButton();
        amountOutboundLabel3 = new javax.swing.JLabel();
        quanityFieldTwo3 = new javax.swing.JTextField();
        removeInvButton3 = new javax.swing.JButton();
        itemNumber3 = new javax.swing.JLabel();
        itemDescripCusFab = new javax.swing.JLabel();
        itemCostCusFab = new javax.swing.JLabel();
        itemPriceCusFab = new javax.swing.JLabel();
        quantityCF = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        bottlesList2 = new javax.swing.JList();
        clearBottles2 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        amountInboundLabel4 = new javax.swing.JLabel();
        quanityFieldOne4 = new javax.swing.JTextField();
        AddInvButton4 = new javax.swing.JButton();
        amountOutboundLabel4 = new javax.swing.JLabel();
        quanityFieldTwo4 = new javax.swing.JTextField();
        removeInvButton4 = new javax.swing.JButton();
        itemNumber4 = new javax.swing.JLabel();
        itemDescripFans = new javax.swing.JLabel();
        itemCostFans = new javax.swing.JLabel();
        itemPriceFans = new javax.swing.JLabel();
        quantityF = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        bottlesList3 = new javax.swing.JList();
        clearBottles3 = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        amountInboundLabel5 = new javax.swing.JLabel();
        quanityFieldOne5 = new javax.swing.JTextField();
        AddInvButton5 = new javax.swing.JButton();
        amountOutboundLabel5 = new javax.swing.JLabel();
        quanityFieldTwo5 = new javax.swing.JTextField();
        removeInvButton5 = new javax.swing.JButton();
        itemNumber5 = new javax.swing.JLabel();
        itemDescripStents = new javax.swing.JLabel();
        itemCostStents = new javax.swing.JLabel();
        itemPriceStents = new javax.swing.JLabel();
        quantitySt = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        bottlesList4 = new javax.swing.JList();
        clearBottles4 = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        amountInboundLabel6 = new javax.swing.JLabel();
        quanityFieldOne6 = new javax.swing.JTextField();
        AddInvButton6 = new javax.swing.JButton();
        amountOutboundLabel6 = new javax.swing.JLabel();
        quanityFieldTwo6 = new javax.swing.JTextField();
        removeInvButton6 = new javax.swing.JButton();
        itemNumber6 = new javax.swing.JLabel();
        itemDescripStorage = new javax.swing.JLabel();
        itemCostStorage = new javax.swing.JLabel();
        itemPriceStorage = new javax.swing.JLabel();
        quantityS = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        bottlesList5 = new javax.swing.JList();
        clearBottles5 = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        transactionId = new javax.swing.JLabel();
        itemNumber8 = new javax.swing.JLabel();
        quantityTrans = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        transactionsList = new javax.swing.JList();

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        jLabel9.setText("Amount Inbound");

        jTextField3.setText("Quanity");
        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });

        jButton3.setText("Add to Inventory");

        jLabel10.setText("Amount Outbound");

        jTextField4.setText("Quanity");

        jButton4.setText("Remove From Inventory");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel11.setText("Item Number:");

        jLabel12.setText("Item Description:");

        jLabel13.setText("Item Cost:");

        jLabel14.setText("Item Price:");

        jLabel15.setText("Quantity:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextField3))
                    .addComponent(jLabel9))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(jButton4)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(98, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel15)
                    .addComponent(jLabel14)
                    .addComponent(jLabel11)
                    .addComponent(jLabel13)
                    .addComponent(jLabel12))
                .addGap(142, 142, 142))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(jButton4))
                .addGap(35, 35, 35))
        );

        transTab.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                transTabFocusGained(evt);
            }
        });

        amountInboundLabel.setText("Amount Inbound");

        quanityFieldOne.setToolTipText("Enter Quanity");
        quanityFieldOne.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quanityFieldOneActionPerformed(evt);
            }
        });

        AddInvButton.setText("Add to Inventory");
        AddInvButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddInvButtonActionPerformed(evt);
            }
        });

        amountOutboundLabel.setText("Amount Outbound");

        removeInvButton.setText("Remove From Inventory");
        removeInvButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeInvButtonActionPerformed(evt);
            }
        });

        itemNumber.setText("Item Number: ");
        itemNumber.setToolTipText("");

        itemDescripBottles.setText("Item Description:");

        itemCostBottles.setText("Item Cost:");

        itemPriceBottle.setText("Item Price:");

        quantityB.setText("Quantity:");

        clearBottles.setText("Clear");

        bottlesList.setModel(new javax.swing.AbstractListModel() {
            String[] bottleListItems = { "White Bottle w/ Screw Lid 1oz", "White Bottle w/ Screw Lid 3oz",
                "White Bottle w/ Screw Lid 6oz", "Pink Bottle w/ Screw Lid 8oz", "Blue Bottle w/ Screw Lid 1oz" };
            public int getSize() { return bottleListItems.length; }
            public Object getElementAt(int i) { return bottleListItems[i]; }

        }
    );
    jScrollPane2.setViewportView(bottlesList);

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(AddInvButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(quanityFieldOne))
                .addComponent(amountInboundLabel))
            .addGap(18, 18, 18)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(amountOutboundLabel)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(removeInvButton)
                    .addGap(18, 18, 18)
                    .addComponent(clearBottles))
                .addComponent(quanityFieldTwo, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(205, Short.MAX_VALUE))
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 121, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(quantityB)
                .addComponent(itemPriceBottle)
                .addComponent(itemNumber)
                .addComponent(itemCostBottles)
                .addComponent(itemDescripBottles))
            .addGap(142, 142, 142))
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(itemNumber)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(itemDescripBottles)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(itemCostBottles)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(itemPriceBottle)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(quantityB)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
                    .addGap(18, 18, 18)))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(amountInboundLabel)
                .addComponent(amountOutboundLabel))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(quanityFieldOne, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(quanityFieldTwo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(AddInvButton)
                .addComponent(removeInvButton)
                .addComponent(clearBottles))
            .addGap(35, 35, 35))
    );

    transTab.addTab("Bottles", jPanel1);

    amountInboundLabel2.setText("Amount Inbound");

    quanityFieldOne2.setText("Quanity");
    quanityFieldOne2.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            quanityFieldOne2ActionPerformed(evt);
        }
    });

    AddInvButton2.setText("Add to Inventory");
    AddInvButton2.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            AddInvButton2ActionPerformed(evt);
        }
    });

    amountOutboundLabel2.setText("Amount Outbound");

    quanityFieldTwo2.setText("Quanity");

    removeInvButton2.setText("Remove From Inventory");
    removeInvButton2.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            removeInvButton2ActionPerformed(evt);
        }
    });

    itemNumber2.setText("Item Number:");

    itemDescripLids.setText("Item Description:");

    itemCostLids.setText("Item Cost:");

    itemPriceLids.setText("Item Price:");

    quantityBL.setText("Quantity:");

    bottlesList1.setModel(new javax.swing.AbstractListModel() {
        String[] bottleListItems = { " Opaque Bottle w/ embossed log", "Clear Plastic Bottle (Custom)",
            " Pyramid Bottle (Custom) 680ml", "Clear Plastic Bottle (Custom)", "Clear Plastic Bottle (Custom)" };
        public int getSize() { return bottleListItems.length; }
        public Object getElementAt(int i) { return bottleListItems[i]; }

    }
    );
    jScrollPane3.setViewportView(bottlesList1);

    clearBottles1.setText("Clear");

    javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
    jPanel3.setLayout(jPanel3Layout);
    jPanel3Layout.setHorizontalGroup(
        jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel3Layout.createSequentialGroup()
            .addGap(24, 24, 24)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(AddInvButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(quanityFieldOne2))
                .addComponent(amountInboundLabel2))
            .addGap(18, 18, 18)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(amountOutboundLabel2)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addComponent(removeInvButton2)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(clearBottles1))
                .addComponent(quanityFieldTwo2, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(207, Short.MAX_VALUE))
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(quantityBL)
                .addComponent(itemPriceLids)
                .addComponent(itemNumber2)
                .addComponent(itemCostLids)
                .addComponent(itemDescripLids))
            .addGap(142, 142, 142))
    );
    jPanel3Layout.setVerticalGroup(
        jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel3Layout.createSequentialGroup()
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(itemNumber2)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(itemDescripLids)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(itemCostLids)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(itemPriceLids)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(quantityBL)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE))
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(amountInboundLabel2)
                .addComponent(amountOutboundLabel2))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(quanityFieldOne2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(quanityFieldTwo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(AddInvButton2)
                .addComponent(removeInvButton2)
                .addComponent(clearBottles1))
            .addGap(35, 35, 35))
    );

    transTab.addTab("Bottles with Lids", jPanel3);

    amountInboundLabel3.setText("Amount Inbound");

    quanityFieldOne3.setText("Quanity");
    quanityFieldOne3.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            quanityFieldOne3ActionPerformed(evt);
        }
    });

    AddInvButton3.setText("Add to Inventory");

    amountOutboundLabel3.setText("Amount Outbound");

    quanityFieldTwo3.setText("Quanity");

    removeInvButton3.setText("Remove From Inventory");
    removeInvButton3.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            removeInvButton3ActionPerformed(evt);
        }
    });

    itemNumber3.setText("Item Number:");

    itemDescripCusFab.setText("Item Description:");

    itemCostCusFab.setText("Item Cost:");

    itemPriceCusFab.setText("Item Price:");

    quantityCF.setText("Quantity:");

    bottlesList2.setModel(new javax.swing.AbstractListModel() {
        String[] bottleListItems = { "Cooling Fan 120mm", "Cooling Fan 250mm ", "Cooling Fan 80mm ",
            "Cooling Fan 140mm", "Desk Fan 100mm" };
        public int getSize() { return bottleListItems.length; }
        public Object getElementAt(int i) { return bottleListItems[i]; }

    }
    );
    jScrollPane4.setViewportView(bottlesList2);

    clearBottles2.setText("Clear");

    javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
    jPanel4.setLayout(jPanel4Layout);
    jPanel4Layout.setHorizontalGroup(
        jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel4Layout.createSequentialGroup()
            .addGap(24, 24, 24)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(AddInvButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(quanityFieldOne3))
                .addComponent(amountInboundLabel3))
            .addGap(18, 18, 18)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(amountOutboundLabel3)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addComponent(removeInvButton3)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(clearBottles2))
                .addComponent(quanityFieldTwo3, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(203, Short.MAX_VALUE))
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(quantityCF)
                .addComponent(itemPriceCusFab)
                .addComponent(itemNumber3)
                .addComponent(itemCostCusFab)
                .addComponent(itemDescripCusFab))
            .addGap(142, 142, 142))
    );
    jPanel4Layout.setVerticalGroup(
        jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel4Layout.createSequentialGroup()
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(itemNumber3)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(itemDescripCusFab)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(itemCostCusFab)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(itemPriceCusFab)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(quantityCF)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE))
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(amountInboundLabel3)
                .addComponent(amountOutboundLabel3))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(quanityFieldOne3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(quanityFieldTwo3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(AddInvButton3)
                .addComponent(removeInvButton3)
                .addComponent(clearBottles2))
            .addGap(35, 35, 35))
    );

    transTab.addTab("Custom Fabrication", jPanel4);

    amountInboundLabel4.setText("Amount Inbound");

    quanityFieldOne4.setText("Quanity");
    quanityFieldOne4.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            quanityFieldOne4ActionPerformed(evt);
        }
    });

    AddInvButton4.setText("Add to Inventory");

    amountOutboundLabel4.setText("Amount Outbound");

    quanityFieldTwo4.setText("Quanity");

    removeInvButton4.setText("Remove From Inventory");
    removeInvButton4.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            removeInvButton4ActionPerformed(evt);
        }
    });

    itemNumber4.setText("Item Number:");

    itemDescripFans.setText("Item Description:");

    itemCostFans.setText("Item Cost:");

    itemPriceFans.setText("Item Price:");

    quantityF.setText("Quantity:");

    bottlesList3.setModel(new javax.swing.AbstractListModel() {
        String[] bottleListItems = { "Cooling Fan 120mm", "Cooling Fan 250mm ", "Cooling Fan 80mm ",
            "Cooling Fan 140mm", "Desk Fan 100mm" };
        public int getSize() { return bottleListItems.length; }
        public Object getElementAt(int i) { return bottleListItems[i]; }

    }
    );
    jScrollPane8.setViewportView(bottlesList3);

    clearBottles3.setText("Clear");

    javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
    jPanel6.setLayout(jPanel6Layout);
    jPanel6Layout.setHorizontalGroup(
        jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel6Layout.createSequentialGroup()
            .addGap(24, 24, 24)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(AddInvButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(quanityFieldOne4))
                .addComponent(amountInboundLabel4))
            .addGap(18, 18, 18)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(amountOutboundLabel4)
                .addGroup(jPanel6Layout.createSequentialGroup()
                    .addComponent(removeInvButton4)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(clearBottles3))
                .addComponent(quanityFieldTwo4, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(207, Short.MAX_VALUE))
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
            .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(quantityF)
                .addComponent(itemPriceFans)
                .addComponent(itemNumber4)
                .addComponent(itemCostFans)
                .addComponent(itemDescripFans))
            .addGap(142, 142, 142))
    );
    jPanel6Layout.setVerticalGroup(
        jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel6Layout.createSequentialGroup()
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel6Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(itemNumber4)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(itemDescripFans)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(itemCostFans)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(itemPriceFans)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(quantityF)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, 60, Short.MAX_VALUE))
                .addGroup(jPanel6Layout.createSequentialGroup()
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(amountInboundLabel4)
                .addComponent(amountOutboundLabel4))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(quanityFieldOne4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(quanityFieldTwo4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(AddInvButton4)
                .addComponent(removeInvButton4)
                .addComponent(clearBottles3))
            .addGap(35, 35, 35))
    );

    transTab.addTab("Fans", jPanel6);

    amountInboundLabel5.setText("Amount Inbound");

    quanityFieldOne5.setText("Quanity");
    quanityFieldOne5.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            quanityFieldOne5ActionPerformed(evt);
        }
    });

    AddInvButton5.setText("Add to Inventory");

    amountOutboundLabel5.setText("Amount Outbound");

    quanityFieldTwo5.setText("Quanity");

    removeInvButton5.setText("Remove From Inventory");
    removeInvButton5.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            removeInvButton5ActionPerformed(evt);
        }
    });

    itemNumber5.setText("Item Number:");

    itemDescripStents.setText("Item Description:");

    itemCostStents.setText("Item Cost:");

    itemPriceStents.setText("Item Price:");

    quantitySt.setText("Quantity:");

    bottlesList4.setModel(new javax.swing.AbstractListModel() {
        String[] bottleListItems = { "Coronary Stent", "Ureteral Stent",
            "Prostatic Stent", "Vascular Stent"};
        public int getSize() { return bottleListItems.length; }
        public Object getElementAt(int i) { return bottleListItems[i]; }

    }
    );
    jScrollPane6.setViewportView(bottlesList4);

    clearBottles4.setText("Clear");

    javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
    jPanel7.setLayout(jPanel7Layout);
    jPanel7Layout.setHorizontalGroup(
        jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel7Layout.createSequentialGroup()
            .addGap(24, 24, 24)
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(AddInvButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(quanityFieldOne5))
                .addComponent(amountInboundLabel5))
            .addGap(18, 18, 18)
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(amountOutboundLabel5)
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addComponent(removeInvButton5)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(clearBottles4))
                .addComponent(quanityFieldTwo5, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(203, Short.MAX_VALUE))
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
            .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(quantitySt)
                .addComponent(itemPriceStents)
                .addComponent(itemNumber5)
                .addComponent(itemCostStents)
                .addComponent(itemDescripStents))
            .addGap(142, 142, 142))
    );
    jPanel7Layout.setVerticalGroup(
        jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel7Layout.createSequentialGroup()
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(itemNumber5)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(itemDescripStents)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(itemCostStents)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(itemPriceStents)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(quantitySt)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE))
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(amountInboundLabel5)
                .addComponent(amountOutboundLabel5))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(quanityFieldOne5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(quanityFieldTwo5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(AddInvButton5)
                .addComponent(removeInvButton5)
                .addComponent(clearBottles4))
            .addGap(35, 35, 35))
    );

    transTab.addTab("Stents", jPanel7);

    amountInboundLabel6.setText("Amount Inbound");

    quanityFieldOne6.setText("Quanity");
    quanityFieldOne6.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            quanityFieldOne6ActionPerformed(evt);
        }
    });

    AddInvButton6.setText("Add to Inventory");

    amountOutboundLabel6.setText("Amount Outbound");

    quanityFieldTwo6.setText("Quanity");

    removeInvButton6.setText("Remove From Inventory");
    removeInvButton6.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            removeInvButton6ActionPerformed(evt);
        }
    });

    itemNumber6.setText("Item Number:");

    itemDescripStorage.setText("Item Description:");

    itemCostStorage.setText("Item Cost:");

    itemPriceStorage.setText("Item Price:");

    quantityS.setText("Quantity:");

    bottlesList5.setModel(new javax.swing.AbstractListModel() {
        String[] bottleListItems = { "Opaque Bottle w/ embossed logo (Custom) 12oz", "Item 2", "Item 3", "Item 4", "Item 5" };
        public int getSize() { return bottleListItems.length; }
        public Object getElementAt(int i) { return bottleListItems[i]; }

    }
    );
    jScrollPane7.setViewportView(bottlesList5);

    clearBottles5.setText("Clear");

    javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
    jPanel8.setLayout(jPanel8Layout);
    jPanel8Layout.setHorizontalGroup(
        jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel8Layout.createSequentialGroup()
            .addGap(24, 24, 24)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(AddInvButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(quanityFieldOne6))
                .addComponent(amountInboundLabel6))
            .addGap(18, 18, 18)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(amountOutboundLabel6)
                .addGroup(jPanel8Layout.createSequentialGroup()
                    .addComponent(removeInvButton6)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(clearBottles5))
                .addComponent(quanityFieldTwo6, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(207, Short.MAX_VALUE))
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
            .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(quantityS)
                .addComponent(itemPriceStorage)
                .addComponent(itemNumber6)
                .addComponent(itemCostStorage)
                .addComponent(itemDescripStorage))
            .addGap(142, 142, 142))
    );
    jPanel8Layout.setVerticalGroup(
        jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel8Layout.createSequentialGroup()
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel8Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(itemNumber6)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(itemDescripStorage)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(itemCostStorage)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(itemPriceStorage)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(quantityS)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE))
                .addGroup(jPanel8Layout.createSequentialGroup()
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(amountInboundLabel6)
                .addComponent(amountOutboundLabel6))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(quanityFieldOne6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(quanityFieldTwo6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(AddInvButton6)
                .addComponent(removeInvButton6)
                .addComponent(clearBottles5))
            .addGap(35, 35, 35))
    );

    transTab.addTab("Storage", jPanel8);

    transactionId.setText("TransAction ID:");

    itemNumber8.setText("Item Number:");

    quantityTrans.setText("Quantity:");

    transactionsList.setModel(new javax.swing.AbstractListModel() {
        String[] strings = { "Transaction 1", "Transaction 2", "Transaction 3", "Transaction 4",
            "Transaction 5", "Transaction 6", "Transaction 7", "Transaction 8", "Transaction 9", "Transaction 10",
            "Transaction 11", "Transaction 12", "Transaction 13", "Transaction 14",
            "Transaction 15", "Transaction 16", "Transaction 17", "Transaction 18", "Transaction 19", "Transaction 20"};
        public int getSize() { return strings.length; }
        public Object getElementAt(int i) { return strings[i]; }
    });
    jScrollPane1.setViewportView(transactionsList);

    javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
    jPanel9.setLayout(jPanel9Layout);
    jPanel9Layout.setHorizontalGroup(
        jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
            .addContainerGap(357, Short.MAX_VALUE)
            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(quantityTrans)
                .addComponent(transactionId)
                .addComponent(itemNumber8))
            .addGap(142, 142, 142))
        .addGroup(jPanel9Layout.createSequentialGroup()
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 267, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(0, 309, Short.MAX_VALUE))
    );
    jPanel9Layout.setVerticalGroup(
        jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel9Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(transactionId)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(itemNumber8)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(quantityTrans)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
            .addContainerGap())
    );

    transTab.addTab("Transactions", jPanel9);

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(transTab)
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(transTab)
            .addContainerGap())
    );
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton4ActionPerformed

    private void removeInvButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeInvButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_removeInvButtonActionPerformed

    private void quanityFieldOneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quanityFieldOneActionPerformed

    }//GEN-LAST:event_quanityFieldOneActionPerformed

    private void quanityFieldOne2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quanityFieldOne2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_quanityFieldOne2ActionPerformed

    private void removeInvButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeInvButton2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_removeInvButton2ActionPerformed

    private void quanityFieldOne3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quanityFieldOne3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_quanityFieldOne3ActionPerformed

    private void removeInvButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeInvButton3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_removeInvButton3ActionPerformed

    private void quanityFieldOne4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quanityFieldOne4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_quanityFieldOne4ActionPerformed

    private void removeInvButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeInvButton4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_removeInvButton4ActionPerformed

    private void quanityFieldOne5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quanityFieldOne5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_quanityFieldOne5ActionPerformed

    private void removeInvButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeInvButton5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_removeInvButton5ActionPerformed

    private void quanityFieldOne6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quanityFieldOne6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_quanityFieldOne6ActionPerformed

    private void removeInvButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeInvButton6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_removeInvButton6ActionPerformed

    private void AddInvButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddInvButton2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_AddInvButton2ActionPerformed

    private void AddInvButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddInvButtonActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_AddInvButtonActionPerformed

    private void transTabFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_transTabFocusGained
     
    }//GEN-LAST:event_transTabFocusGained

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AddInvButton;
    private javax.swing.JButton AddInvButton2;
    private javax.swing.JButton AddInvButton3;
    private javax.swing.JButton AddInvButton4;
    private javax.swing.JButton AddInvButton5;
    private javax.swing.JButton AddInvButton6;
    private javax.swing.JLabel amountInboundLabel;
    private javax.swing.JLabel amountInboundLabel2;
    private javax.swing.JLabel amountInboundLabel3;
    private javax.swing.JLabel amountInboundLabel4;
    private javax.swing.JLabel amountInboundLabel5;
    private javax.swing.JLabel amountInboundLabel6;
    private javax.swing.JLabel amountOutboundLabel;
    private javax.swing.JLabel amountOutboundLabel2;
    private javax.swing.JLabel amountOutboundLabel3;
    private javax.swing.JLabel amountOutboundLabel4;
    private javax.swing.JLabel amountOutboundLabel5;
    private javax.swing.JLabel amountOutboundLabel6;
    public javax.swing.JList bottlesList;
    public javax.swing.JList bottlesList1;
    public javax.swing.JList bottlesList2;
    public javax.swing.JList bottlesList3;
    public javax.swing.JList bottlesList4;
    public javax.swing.JList bottlesList5;
    private javax.swing.JButton clearBottles;
    private javax.swing.JButton clearBottles1;
    private javax.swing.JButton clearBottles2;
    private javax.swing.JButton clearBottles3;
    private javax.swing.JButton clearBottles4;
    private javax.swing.JButton clearBottles5;
    public javax.swing.JLabel itemCostBottles;
    private javax.swing.JLabel itemCostCusFab;
    private javax.swing.JLabel itemCostFans;
    private javax.swing.JLabel itemCostLids;
    private javax.swing.JLabel itemCostStents;
    private javax.swing.JLabel itemCostStorage;
    public javax.swing.JLabel itemDescripBottles;
    private javax.swing.JLabel itemDescripCusFab;
    private javax.swing.JLabel itemDescripFans;
    private javax.swing.JLabel itemDescripLids;
    private javax.swing.JLabel itemDescripStents;
    private javax.swing.JLabel itemDescripStorage;
    public javax.swing.JLabel itemNumber;
    private javax.swing.JLabel itemNumber2;
    private javax.swing.JLabel itemNumber3;
    private javax.swing.JLabel itemNumber4;
    private javax.swing.JLabel itemNumber5;
    private javax.swing.JLabel itemNumber6;
    private javax.swing.JLabel itemNumber8;
    public javax.swing.JLabel itemPriceBottle;
    private javax.swing.JLabel itemPriceCusFab;
    private javax.swing.JLabel itemPriceFans;
    private javax.swing.JLabel itemPriceLids;
    private javax.swing.JLabel itemPriceStents;
    private javax.swing.JLabel itemPriceStorage;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField quanityFieldOne;
    private javax.swing.JTextField quanityFieldOne2;
    private javax.swing.JTextField quanityFieldOne3;
    private javax.swing.JTextField quanityFieldOne4;
    private javax.swing.JTextField quanityFieldOne5;
    private javax.swing.JTextField quanityFieldOne6;
    private javax.swing.JTextField quanityFieldTwo;
    private javax.swing.JTextField quanityFieldTwo2;
    private javax.swing.JTextField quanityFieldTwo3;
    private javax.swing.JTextField quanityFieldTwo4;
    private javax.swing.JTextField quanityFieldTwo5;
    private javax.swing.JTextField quanityFieldTwo6;
    public javax.swing.JLabel quantityB;
    private javax.swing.JLabel quantityBL;
    private javax.swing.JLabel quantityCF;
    private javax.swing.JLabel quantityF;
    private javax.swing.JLabel quantityS;
    private javax.swing.JLabel quantitySt;
    private javax.swing.JLabel quantityTrans;
    private javax.swing.JButton removeInvButton;
    private javax.swing.JButton removeInvButton2;
    private javax.swing.JButton removeInvButton3;
    private javax.swing.JButton removeInvButton4;
    private javax.swing.JButton removeInvButton5;
    private javax.swing.JButton removeInvButton6;
    private javax.swing.JTabbedPane transTab;
    private javax.swing.JLabel transactionId;
    private javax.swing.JList transactionsList;
    // End of variables declaration//GEN-END:variables

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Object obj = e.getSource();
            MainModel mm = new MainModel();
            if (obj == AddInvButton) {
                String source = " bottles ";
                switch(bottlesList.getSelectedIndex()){
                    case 0: inumber = 4010001;
                        break;
                    case 1: inumber = 4010002;
                        break;
                    case 2: inumber = 4010003;
                        break;
                    case 3: inumber = 4020004;
                        break;
                    case 4: inumber = 4030001;
                }
                String senderString = quanityFieldOne.getText();
                mm.setBottleIncreaseInv(senderString, source, inumber);
                mm.setTransaction(inumber, senderString);

            } else if (obj == removeInvButton) {
                 switch(bottlesList.getSelectedIndex()){
                    case 0: inumber = 4010001;
                        break;
                    case 1: inumber = 4010002;
                        break;
                    case 2: inumber = 4010003;
                        break;
                    case 3: inumber = 4020004;
                        break;
                    case 4: inumber = 4030001;
                }
                String senderString = quanityFieldTwo.getText();
                String source = " bottles ";
                mm.setDecreaseInv(senderString,source, inumber);
            } else if (obj == AddInvButton2) {
                 switch(bottlesList1.getSelectedIndex()){
                    case 0: inumber = 3010001;
                        break;
                    case 1: inumber = 3010002;
                        break;
                    case 2: inumber = 3020001;
                        break;
                    case 3: inumber = 3030001;
                        break;
                    case 4: inumber = 3040001;
                }
                String source = " bottleswlids ";
                String senderString = quanityFieldOne2.getText();
                mm.setBottleIncreaseInv(senderString, source, inumber);
            } else if (obj == removeInvButton2) {
                switch(bottlesList1.getSelectedIndex()){
                    case 0: inumber = 3010001;
                        break;
                    case 1: inumber = 3010002;
                        break;
                    case 2: inumber = 3020001;
                        break;
                    case 3: inumber = 3030001;
                        break;
                    case 4: inumber = 3040001;
                }
                String source = " bottleswlids ";
                String senderString = quanityFieldTwo2.getText();
                mm.setDecreaseInv(senderString, source, inumber);
            } else if (obj == AddInvButton3) {
                switch(bottlesList2.getSelectedIndex()){
                    case 0: inumber = 3010001;
                        break;
                    case 1: inumber = 3010002;
                        break;
                    case 2: inumber = 3020001;
                        break;
                    case 3: inumber = 3030001;
                        break;
                    case 4: inumber = 3040001;
                }
                String source = " customfabrication ";
                String senderString = quanityFieldOne3.getText();
                inumber = 4010001;
                mm.setBottleIncreaseInv(senderString, source, inumber);
            } else if (obj == removeInvButton3) {
                switch(bottlesList2.getSelectedIndex()){
                    case 0: inumber = 3010001;
                        break;
                    case 1: inumber = 3010002;
                        break;
                    case 2: inumber = 3020001;
                        break;
                    case 3: inumber = 3030001;
                        break;
                    case 4: inumber = 3040001;
                }
                String senderString = quanityFieldTwo3.getText();
                String source = " customfabrication ";
                inumber = 4010001;
                mm.setDecreaseInv(senderString, source, inumber);
            } else if (obj == AddInvButton4) {
                switch(bottlesList3.getSelectedIndex()){
                    case 0: inumber = 1010001;
                        break;
                    case 1: inumber = 1010002;
                        break;
                    case 2: inumber = 1010003;
                        break;
                    case 3: inumber = 1010004;
                        break;
                    case 4: inumber = 1010005;
                }
                String senderString = quanityFieldOne4.getText();
                String source = " fans ";
                mm.setBottleIncreaseInv(senderString, source, inumber);
            } else if (obj == removeInvButton4) {
                switch(bottlesList3.getSelectedIndex()){
                    case 0: inumber = 1010001;
                        break;
                    case 1: inumber = 1010002;
                        break;
                    case 2: inumber = 1010003;
                        break;
                    case 3: inumber = 1010004;
                        break;
                    case 4: inumber = 1010005;
                }
                String source = " fans ";
                String senderString = quanityFieldTwo4.getText();
                mm.setDecreaseInv(senderString, source, inumber);
            } else if (obj == AddInvButton5) {
                 switch(bottlesList4.getSelectedIndex()){
                    case 0: inumber = 2010001;
                        break;
                    case 1: inumber = 2020001;
                        break;
                    case 2: inumber = 2030001;
                        break;
                    case 3: inumber = 2040001;
                        break;
                    case 4: inumber = 4020003;
                }
                String source = " stents ";
                String senderString = quanityFieldOne5.getText();
                mm.setBottleIncreaseInv(senderString, source, inumber);

            } else if (obj == removeInvButton5) {
                switch(bottlesList4.getSelectedIndex()){
                    case 0: inumber = 2010001;
                        break;
                    case 1: inumber = 2020001;
                        break;
                    case 2: inumber = 2030001;
                        break;
                    case 3: inumber = 2040001;
                        break;
                    case 4: inumber = 4020003;
                }
                String senderString = quanityFieldTwo5.getText();
                String source = " stents ";
                mm.setDecreaseInv(senderString, source, inumber);

            } else if (obj == AddInvButton6) {
                String source = " storage ";
                String senderString = quanityFieldOne5.getText();
                mm.setBottleIncreaseInv(senderString, source, inumber);

            } else if (obj == removeInvButton6) {
                String senderString = quanityFieldTwo5.getText();
                String source = " storage ";
                mm.setDecreaseInv(senderString, source, inumber);

            }else if(obj == clearBottles){
                quanityFieldOne.setText("");
                quanityFieldTwo.setText("");
                itemNumber.setText("");
                itemDescripBottles.setText("");
                itemCostBottles.setText("");
                itemPriceBottle.setText("");
                quantityB.setText("");
                
            }else if(obj == clearBottles1){
                quanityFieldOne.setText("");
                quanityFieldTwo.setText("");
                itemNumber.setText("");
                itemDescripBottles.setText("");
                itemCostBottles.setText("");
                itemPriceBottle.setText("");
                quantityB.setText("");
            }else if(obj == clearBottles2){
                quanityFieldOne.setText("");
                quanityFieldTwo.setText("");
                itemNumber.setText("");
                itemDescripBottles.setText("");
                itemCostBottles.setText("");
                itemPriceBottle.setText("");
                quantityB.setText("");
            }else if(obj == clearBottles3){
                quanityFieldOne.setText("");
                quanityFieldTwo.setText("");
                itemNumber.setText("");
                itemDescripBottles.setText("");
                itemCostBottles.setText("");
                itemPriceBottle.setText("");
                quantityB.setText("");
            }else if(obj == clearBottles4){
                quanityFieldOne.setText("");
                quanityFieldTwo.setText("");
                itemNumber.setText("");
                itemDescripBottles.setText("");
                itemCostBottles.setText("");
                itemPriceBottle.setText("");
                quantityB.setText("");
            }else if(obj == clearBottles5){
                quanityFieldOne.setText("");
                quanityFieldTwo.setText("");
                itemNumber.setText("");
                itemDescripBottles.setText("");
                itemCostBottles.setText("");
                itemPriceBottle.setText("");
                quantityB.setText("");
            }

        } catch (NullPointerException err) {
            System.err.println(err);

        }
    }
}
