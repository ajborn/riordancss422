/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Controller.DBController;

/**
 *
 * @author Austin
 */
public class MainModel {
    //Iventory Item Attributes

    private static float itemCost;
    private static String itemDescription;
    private static int itemNumber;
    private static float itemPrice;
    private static int quantity;
    private static int transNumber;
    private static String itemCostS;
    private static String itemNumberS;
    private static String itemPriceS;
    private static String quantityS;
    private static int itemnumber;
    private int listSize = 0;
    private int q;
    //Objects    
    
    public MainModel() {
      
    }

    public void setBottleIncreaseInv(String s, String src, int inum) {
        String source = src;
        itemnumber = inum;
        quantityS = s;
        quantity = Integer.parseInt(quantityS);

        System.out.println(quantity);
        try {
            DBController db = new DBController();
            db.doIncreaseBottleQuantity(quantity, itemnumber, source);
        } catch (NullPointerException e) {
            System.out.println(e);

        }
    }

    public Integer getIncreaseInv() {
        return quantity;
    }

    public void setDecreaseInv(String src, String s, int inum) {
        String source = src;
        itemnumber = inum;
        quantityS = s;
        quantity = Integer.parseInt(source);

        System.out.println(quantity);
        try {
            DBController db = new DBController();
            db.doDecreaseInv(quantity, itemnumber, quantityS);
        } catch (NullPointerException e) {
            System.out.println(e);

        }

    }
   

    public void getItemInformation(int query) {
        q = query;
        System.out.println(q);
        String[] queryList = {"SELECT * FROM bottles WHERE ItemNumber = '4010001'",
            "SELECT * FROM bottles WHERE ItemNumber = '4010002'", "SELECT * FROM bottles WHERE ItemNumber = '4010003'",
            "SELECT * FROM bottles WHERE ItemNumber = '4020004'", "SELECT * FROM bottles WHERE ItemNumber = '4030001'"};
        DBController dbc = new DBController();
        switch (q) {
            case 0:
                dbc.retriveBottleItemInfo(queryList[0]);

                break;
            case 1:
                dbc.retriveBottleItemInfo(queryList[1]);
                break;
            case 2:
                dbc.retriveBottleItemInfo(queryList[2]);
                break;
            case 3:
                dbc.retriveBottleItemInfo(queryList[3]);
                break;
            case 4:
                dbc.retriveBottleItemInfo(queryList[4]);
                break;

        }

    }

    public void getBottleLidInfo(int query) {
        q = query;
        System.out.println(q);
        String[] queryList = {"SELECT * FROM bottleswlids WHERE ItemNumber = '3010001'",
            "SELECT * FROM bottleswlids WHERE ItemNumber = '3010002'", "SELECT * FROM bottleswlids WHERE ItemNumber = '3020001'",
            "SELECT * FROM bottleswlids WHERE ItemNumber = '3030001'", "SELECT * FROM bottleswlids WHERE ItemNumber = '3040001'"};
        DBController dbc = new DBController();
        switch (q) {
            case 0:
                dbc.retrieveLidInfo(queryList[0]);

                break;
            case 1:
                dbc.retrieveLidInfo(queryList[1]);
                break;
            case 2:
                dbc.retrieveLidInfo(queryList[2]);
                break;
            case 3:
                dbc.retrieveLidInfo(queryList[3]);
                break;
            case 4:
                dbc.retrieveLidInfo(queryList[4]);
                break;

        }


    }

    public void getCusFabInfo() {

        DBController dbc = new DBController();
        //dbc.retrieveCusFabInfo();


    }

    public void getFanInfo(int query) {

        q = query;
        System.out.println(q);
        String[] queryList = {"SELECT * FROM fans WHERE ItemNumber = '1010001'",
            "SELECT * FROM fans WHERE ItemNumber = '1010002'", "SELECT * FROM fans WHERE ItemNumber = '1010003'",
            "SELECT * FROM fans WHERE ItemNumber = '1010004'", "SELECT * FROM fans WHERE ItemNumber = '1010005'"};
        DBController dbc = new DBController();
        switch (q) {
            case 0:
                dbc.retrieveFans(queryList[0]);

                break;
            case 1:
                dbc.retrieveFans(queryList[1]);
                break;
            case 2:
                dbc.retrieveFans(queryList[2]);
                break;
            case 3:
                dbc.retrieveFans(queryList[3]);
                break;
            case 4:
                dbc.retrieveFans(queryList[4]);
                break;

        }


    }

    public void getStentInfo(int query) {
        q = query;
        System.out.println(q);
    String[] queryList = {"SELECT * FROM stents WHERE ItemNumber = '2010001'",
            "SELECT * FROM stents WHERE ItemNumber = '2020001'", "SELECT * FROM stents WHERE ItemNumber = '2030001'",
            "SELECT * FROM stents WHERE ItemNumber = '2040001'", "SELECT * FROM stents WHERE ItemNumber = '4020003'"};
        DBController dbc = new DBController();
        switch (q) {
            case 0:
                dbc.retrieveStentInfo(queryList[0]);

                break;
            case 1:
                dbc.retrieveStentInfo(queryList[1]);
                break;
            case 2:
                dbc.retrieveStentInfo(queryList[2]);
                break;
            case 3:
                dbc.retrieveStentInfo(queryList[3]);
                break;
            case 4:
                dbc.retrieveStentInfo(queryList[4]);
                break;

        }
        


    }

    public void getStorageInfo(int query) {
        q = query;
        System.out.println(q);
        String[] queryList = {"SELECT * FROM storage WHERE ItemNumber = '2010001'",
            "SELECT * FROM storage WHERE ItemNumber = '2020001'", "SELECT * FROM storage WHERE ItemNumber = '2030001'",
            "SELECT * FROM storage WHERE ItemNumber = '2040001'", "SELECT * FROM storage WHERE ItemNumber = '4020003'"};
        DBController dbc = new DBController();
        switch (q) {
            case 0:
                dbc.retrieveFans(queryList[0]);

                break;
            case 1:
                dbc.retrieveFans(queryList[1]);
                break;
            case 2:
                dbc.retrieveFans(queryList[2]);
                break;
            case 3:
                dbc.retrieveFans(queryList[3]);
                break;
            case 4:
                dbc.retrieveFans(queryList[4]);
                break;

        }


    }


    public void setItemNumber(int i) {
        itemNumber = i;
    }

    public String getItemNumber() {
        itemNumberS = String.valueOf(itemNumber);
        return itemNumberS;
    }

    public void setItemDescription(String s) {
        itemDescription = s;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemCost(float i) {
        itemCost = i;

    }

    public String getItemCost() {
        try {
            itemCostS = String.valueOf(itemCost);

        } catch (Exception e) {
        }
        System.out.println(itemCostS);
        return itemCostS;
    }

    public void setItemPrice(float i) {
        itemPrice = i;

    }

    public String getItemPrice() {
        itemPriceS = String.valueOf(itemPrice);
        return itemPriceS;
    }

    public void setItemQuantity(int i) {
        quantity = i;

    }
    public void setTransNumber(int i){
        transNumber = i;
    }
    public String getTransNumber(){
        String transAction = String.valueOf(transNumber);
        return transAction;
    }
    public void setTransList(int i){
        listSize = i;
    }
    public Integer getTransListSize(){
        
        return listSize;
    }

    public String getItemQuantity() {
        quantityS = String.valueOf(quantity);
        return quantityS;
    }
    public void getTransactionRow(int val){
        DBController dbc = new DBController();
        dbc.getTransactions(val);
        
    }
    public void setTransaction(int inumber, String senderString) {
        int quant = Integer.parseInt(senderString);
        DBController dbc = new DBController();
        dbc.createTransaction(inumber, quant);
    }

}
